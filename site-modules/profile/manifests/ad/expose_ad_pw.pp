#
# profile::ad::expose_ad_pw
#

class profile::ad::expose_ad_pw {
  $password_domain_admin = "FJERN PASSORDET FRA DENNE FILEN NÅR DU HAR NOTERT DET!\nSEC.CORE\\Administrator\t${lookup('profile::ad::server::password')}"

  file { 'C:/Users/Admin/Documents':
    ensure => directory,
  }

  file { 'C:/Users/Admin/Documents/password-domain-administrator.txt':
    ensure    => file,
    content   => $password_domain_admin,
    mode      => "0600",
    replace   => no,
    show_diff => no,
    require => File['C:/Users/Admin/Documents'],
  }
}
