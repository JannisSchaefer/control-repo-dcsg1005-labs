class role::domain_joined_client {
  include ::profile::base_windows
  include ::profile::dns::client
  include ::profile::ad::client
  include ::profile::ad::expose_ad_pw
}
