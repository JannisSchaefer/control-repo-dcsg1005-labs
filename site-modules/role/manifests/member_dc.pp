class role::member_dc {
  include ::profile::base_windows
  include ::profile::base_windows_server
  include ::profile::dns::client
  include ::profile::ad::member
}
